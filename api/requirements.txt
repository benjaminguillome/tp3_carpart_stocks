fastapi==0.104.1
pymongo==4.6.0
pydantic==2.4.2
uvicorn==0.15.0
