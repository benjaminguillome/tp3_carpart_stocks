from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from pymongo import MongoClient
from typing import List
from bson import ObjectId, json_util
import json

# Initialisation de l'application FastAPI
app = FastAPI()

# Connexion à la base de données MongoDB
client = MongoClient("mongodb://mongo:27017/")
db = client["carpart"]
collection = db["stocks"]

# Modèle Pydantic pour les opérations de création et de modification de produit
class Product(BaseModel):
    name: str
    description: str
    quantity: int

class CustomJsonEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

# Ajoute un produit
@app.post("/add_product")
def add_product(product: Product):
    result = collection.insert_one(product.dict())
    return {"id": str(result.inserted_id)}

# Modifie un produit par son id
@app.put("/update_product/{product_id}")
def update_product(product_id: str, product: Product):
    result = collection.update_one({"_id": ObjectId(product_id)}, {"$set": product.dict()})
    if result.modified_count == 0:
        raise HTTPException(status_code=404, detail="Produit non trouvé")
    return {"message": "Produit mis à jour avec succès"}

# Supprimer un produit par son id
@app.delete("/delete_product/{product_id}")
def delete_product(product_id: str):
    result = collection.delete_one({"_id": ObjectId(product_id)})
    if result.deleted_count == 0:
        raise HTTPException(status_code=404, detail="Produit non trouvé")
    return {"message": "Produit supprimé avec succès"}

# Récupère un produit identifié par son id
@app.get("/get_product/{product_id}")
def get_product(product_id: str):
    product = collection.find_one({"_id": ObjectId(product_id)})
    if not product:
        raise HTTPException(status_code=404, detail="Produit non trouvé")
    else:
        return json.loads(json.dumps(product, cls = CustomJsonEncoder))


# Lancement de l'application avec Uvicorn
if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="127.0.0.1", port=8000)
